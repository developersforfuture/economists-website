<?php

namespace App\Command;

use App\Entity\Subscription;
use App\Service\SubscriptionUtilsService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;

class ManualVerificationCommand extends Command
{
    protected static $defaultName = 'app:verify-manual';

    private $subscriptionUtils;
    private $entityManager;
    private $mailer;
    private $logger;

    public function __construct(
        SubscriptionUtilsService $subscriptionUtils,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        LoggerInterface $logger,
        RouterInterface $router
    ) {
        $this->subscriptionUtils = $subscriptionUtils;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->router = $router;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('This sends a verification mail to manually added subscriptions.')
        ->addOption(
            'dry-run',
            'd',
            InputOption::VALUE_OPTIONAL,
            'Creates the mails and prints it to the console. Default is true, set it to false, to avoid it',
            true
        )
        ->addArgument('domain', InputArgument::REQUIRED, 'Set the domain to generate the urls correctly');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $context = $this->router->getContext();
        $context->setHost($input->getArgument('domain'));
        $context->setScheme('https');

        $output->writeln("Looking for Subscriptions with missing token, assuming these are the ones which were manually added...");

        $manualSubs = $this->entityManager
            ->getRepository(Subscription::class)
            ->findBy(['verificationToken' => '']);

        if (empty($manualSubs)) {
            $output->writeln("<error>There are no Subscriptions with missing verification token.</error>");

            return;
        }

        $output->writeln("Adding tokens and sending verification mails...");

        foreach($manualSubs as $subscription) {
            if ($subscription instanceof Subscription) {
                $token = $this->subscriptionUtils->generateUuid();
                $subscription->setVerificationToken($token);

                $output->writeln("==============================");
                $output->writeln("Subscription ID " . $subscription->getId());
                $output->writeln("Sending mail... ");

                if ($input->getOption('dry-run') !== 'true') {
                    $this->sendTestManualVerificationMail($subscription, $token, $output);
                } else {
                    $this->printTestManualVerificationMail($subscription, $token, $output);
                }

                $this->entityManager->persist($subscription);

                $output->writeln("==============================");
            }
        }

        $this->entityManager->flush();

        return;
    }

    private function printTestManualVerificationMail(Subscription $subscription, string $token, OutputInterface $output): void
    {
        $text = $this->subscriptionUtils->buildTextMailContent($subscription, $token);

        $output->writeln($text);
    }

    private function sendTestManualVerificationMail(Subscription $subscription, string $token, OutputInterface $output): void
    {
        try {
            $result = $this->subscriptionUtils->sendVerificationMail($this->mailer, $subscription, $token);

        if ($result) {
            $output->writeln('Mail sent.');
            $this->logger->info('Mail sent.');

            return;
        }

        } catch (\Swift_TransportException $e) {
            $output->writeln("Sending Mail failed, see log for more information. ");
            $this->logger->error(
                'Problems to send mail',
                [
                    'message' => $e->getMessage(),
                    'subscription_email' => $subscription->getEmailAddress(),
                    'subscription' => $subscription->toArray(),
                ]
            );

            return;
        }


        $output->writeln("Sending Mail failed, unknown error.");
    }

}
<?php

namespace App\Command;

use App\Entity\Subscription;
use App\Service\SubscriptionUtilsService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\RouterInterface;

class ImportFromCSVCommand extends Command
{
    protected static $defaultName = 'app:import-csv';

    private $entityManager;

    public function __construct(
        SubscriptionUtilsService $subscriptionUtils,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        LoggerInterface $logger,
        RouterInterface $router
    ) {
        $this->subscriptionUtils = $subscriptionUtils;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->router = $router;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('imports a csv based on a given order of columns')
        ->addArgument('file', InputArgument::REQUIRED, 'Set the file to read relative the project root folder');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = __DIR__.'/../../'.$input->getArgument('file');
        if (!file_exists($path)) {
            $output->writeln("<error>There are no Subscriptions with missing verification token.</error>");
        }

        $handle = fopen($path, 'r');
        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
            $subscription = new Subscription();
            $subscription->setCreatedDate(new \DateTime($data[1], new \DateTimeZone('Europe/Berlin')));
            $subscription->setFirstName($data[2]);
            $subscription->setLastName($data[3]);
            $subscription->setEmailAddress($data[4]);
            $subscription->setCapacityOfSigning($data[5]);
            $subscription->setCurrentStatus($data[6]);
            $subscription->setAreaOfStudyWork($data[7]);
            $subscription->setKindOfInvolvement(explode(',', $data[8]));
            $subscription->setTitle($data[9]);

            $subscription->setIsDataPolicyAccepted(true);
            $subscription->setIsVerified(false);
            $subscription->setCity('');
            $subscription->setCountry('');

            $this->entityManager->persist($subscription);
        }

        $this->entityManager->flush();
    }
}
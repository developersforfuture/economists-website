<?php

namespace App\Controller\Campaign;

use App\Entity\Subscription;
use App\Form\Type\SubscriptionType;
use App\Service\SubscriptionUtilsService;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class SubscriptionController extends AbstractController
{
    public function index(
        Request $request,
        EntityManagerInterface $entityManager,
        \Swift_Mailer $mailer,
        LoggerInterface $logger,
        SubscriptionUtilsService $subscriptionUtils
    ): Response {
        $subscription = new Subscription();
        $form = $this->createForm(SubscriptionType::class, $subscription);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $token = $subscriptionUtils->generateUuid();

            $logger->info('Verification token created: ' . $token);
            /** @var Subscription $subscription */
            $subscription = $form->getData();
            $existingSubscription = $entityManager->getRepository(Subscription::class)
                ->findOneBy(['emailAddress' => $subscription->getEmailAddress()]);
            if ($existingSubscription instanceof Subscription) {
                $logger->warning('Second try with same email address');

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'errors' => ['Mail-Address already exists: ' . $subscription->getEmailAddress()],
                    ]
                );
            }
            $subscription->setIsVerified(false);
            $subscription->setVerificationToken($token);
            $subscription->setCreatedDate(new \DateTime('now'));
            $entityManager->persist($subscription);
            $entityManager->flush();

            try {
                $result = $subscriptionUtils->sendVerificationMail($mailer, $subscription, $token);
            } catch (\Swift_TransportException $e) {
                $logger->error(
                    'Problems to send mail',
                    [
                        'message' => $e->getMessage(),
                        'subscription_email' => $subscription->getEmailAddress(),
                        'subscription' => $subscription->toArray(),
                    ]
                );

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'errors' => ['Problems to send Mail. Please try again.'],
                    ]
                );
            }
            if ($result) {
                $logger->info('Successful subscription - Mail send');
            } else {
                $logger->error('Problems to send mail');

                return $this->render(
                    'pages/campaign/subscription.html.twig',
                    [
                        'form' => $form->createView(),
                        'errors' => ['Problems to send Mail. Please try again.'],
                    ]
                );
            }

            return $this->redirectToRoute('successful_campaign_subscription');
        }
        
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        
        return $this->render('pages/campaign/subscription.html.twig', ['form' => $form->createView(), 'errors' => $errors]);
    }

    public function successfulSubscription(): Response
    {
        return $this->render('pages/campaign/successful_subscription.html.twig', ['form' => null, 'errors' => []]);
    }
}

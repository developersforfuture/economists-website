<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Entity\Subscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class SubscriptionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => false, 'attr' => ['placeholder' => 'form.label.title'],'required' => false, 'help' => 'form.help.title'])
            ->add('firstName', TextType::class, ['label' => false, 'required' => true, 'attr' => ['placeholder' => 'form.label.firstName']])
            ->add('lastName', TextType::class, ['label' => false, 'attr' => ['placeholder' => 'form.label.lastName'], 'required' => true])
            ->add('emailAddress', EmailType::class, ['label' => false, 'attr' => ['placeholder' => 'form.label.emailAddress'], 'required' => true, 'help' => 'form.help.emailAddress'])
            ->add('city', TextType::class, ['label' => false, 'attr' => ['placeholder' => 'form.label.city'], 'required' => true])
            ->add('country', TextType::class, ['label' => false, 'attr' => ['placeholder' => 'form.label.country'], 'required' => true, 'help' => 'form.help.country'])
            ->add(
                'capacityOfSigning',
                ChoiceType::class,
                [
                    'label' => 'form.label.capacityOfSigning',
                    'required' => true,
                    'choices' => [
                        'form.choice.label.capacityOfSigning.individual' => Subscription::CAPACITY_INDIVIDUAL,
                        'form.choice.label.capacityOfSigning.organization' => Subscription::CAPACITY_ORGANIZATION,
                    ],
                    'data' => Subscription::CAPACITY_INDIVIDUAL,
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add(
                'currentStatus',
                ChoiceType::class,
                [
                    'label' => 'form.label.currentStatus',
                    'required' => false,
                    'choices' => [
                        'form.choice.label.currentStatus.student' => Subscription::CURRENT_STATUS_STUDENT,
                        'form.choice.label.currentStatus.researcher' => Subscription::CURRENT_STATUS_RESEARCHER,
                        'form.choice.label.currentStatus.working_professional' => Subscription::CURRENT_STATUS_WORKING_PROFESIONAL,
                        'form.choice.label.currentStatus.other' => Subscription::CURRENT_STATUS_OTHER,
                    ],
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add(
                'currentStatusOther',
                TextType::class,
                ['label' => false, 'attr' => ['placeholder' => 'form.label.currentStatusOther'], 'required' => false]
            )
            ->add(
                'areaOfStudyWork',
                ChoiceType::class,
                [
                    'label' => 'form.label.areaOfStudyWork',
                    'required' => false,
                    'choices' => [
                        'form.choice.label.areaOfStudyWork.economics' => Subscription::STUDYWORK_ECONOMIST,
                        'form.choice.label.areaOfStudyWork.social_science' => Subscription::STUDYWORK_SOCIAL,
                        'form.choice.label.areaOfStudyWork.humanities' => Subscription::STUDYWORK_HUMANITIES,
                        'form.choice.label.areaOfStudyWork.natural_science' => Subscription::STUDYWORK_NATURAL_SCIENCE,
                        'form.choice.label.areaOfStudyWork.other' => Subscription::STUDYWORK_OTHER,
                    ],
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add(
                'areaOfStudyWorkOther',
                TextType::class,
                ['label' => false, 'attr' => ['placeholder' => 'form.label.areaOfStudyWorkOther'], 'required' => false]
            )
            ->add(
                'professionalOrganizationName',
                TextType::class,
                ['label' => false, 'attr' => ['placeholder' => 'form.label.professionalOrganizationName'], 'required' => false]
            )
            ->add(
                'professionalOrganizationDescription',
                ChoiceType::class,
                [
                    'label' => 'form.label.professionalOrganizationDescription',
                    'required' => false,
                    'choices' => [
                        'form.choice.label.professionalOrganizationDescription.study_network' => Subscription::ORGANIZATION_DESCRIPTION_STUDY_NETWORK,
                        'form.choice.label.professionalOrganizationDescription.university' => Subscription::ORGANIZATION_DESCRIPTION_UNIVERSITY,
                        'form.choice.label.professionalOrganizationDescription.research_institute' => Subscription::ORGANIZATION_DESCRIPTION_RESEARCH_INSTITUTE,
                        'form.choice.label.professionalOrganizationDescription.ngo' => Subscription::ORGANIZATION_DESCRIPTION_STUDY_NGO,
                        'form.choice.label.professionalOrganizationDescription.small_enterprise' => Subscription::ORGANIZATION_DESCRIPTION_SMALL_ENTERPRISE,
                        'form.choice.label.professionalOrganizationDescription.large_enterprise' => Subscription::ORGANIZATION_DESCRIPTION_LARGE_ENTERPRISE,
                        'form.choice.label.professionalOrganizationDescription.other' => Subscription::ORGANIZATION_DESCRIPTION_OTHER,
                    ],
                    'expanded' => true,
                    'multiple' => false,
                ]
            )
            ->add(
                'professionalOrganizationDescriptionOther',
                TextType::class,
                ['label' => false, 'attr' => ['placeholder' => 'form.label.professionalOrganizationDescriptionOther'], 'required' => false]
            )
            ->add(
                'kindOfInvolvement',
                ChoiceType::class,
                [
                    'label' => 'form.label.kindOfInvolvement',
                    'required' => false,
                    'choices' => [
                        'form.choice.label.kindOfInvolvement.informed' => Subscription::KIND_OF_INVOLVEMENT_INFORMED,
                        'form.choice.label.kindOfInvolvement.actively' => Subscription::KIND_OF_INVOLVEMENT_ACTIVELY,
                    ],
                    'expanded' => true,
                    'multiple' => true,
                ]
            )
            ->add(
                'isDataPolicyAccepted',
                CheckboxType::class,
                [
                    'label' => true,
                    'required' => true,
                    'constraints' => [
                        new IsTrue(
                            [
                                'message' => 'I know, it\'s hard, but you must agree to our data policy.',
                            ]
                        ),
                    ]
                ]
            )
            ->add('submit', SubmitType::class, ['label' => 'form.label.submit']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subscription::class,
        ]);
    }
}

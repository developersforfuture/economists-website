<?php

namespace App\Service;

use App\Entity\Subscription;
use Twig\Environment;

class SubscriptionUtilsService
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    public function generateUuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0C2f) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0x2Aff),
            mt_rand(0, 0xffD3),
            mt_rand(0, 0xff4B)
        );
    }

    public function sendVerificationMail(\Swift_Mailer $mailer, Subscription $subscription, string $token): int
    {
        $message = (new \Swift_Message('Verification for Economists for Future Signature'))
            ->setFrom(['verify@mail.econ4future.org' => 'Economists for Future'])
            ->setReplyTo(['info@econ4future.org' => 'Economists for Future'])
            ->setTo($subscription->getEmailAddress())
            ->setBody($this->buildHTMLMailContent($subscription, $token), 'text/html')
            ->addPart($this->buildTextMailContent($subscription, $token), 'text/plain');

        return $mailer->send($message);
    }

    public function buildHTMLMailContent(Subscription $subscription, string $token): string
    {
        return $this->twig->render( // TODO Exception Handling?
            'pages/campaign/email_verification.html.twig',
            [
                'subscription' => $subscription,
                'token' => $token,
            ]
            );
    }

    public function buildTextMailContent(Subscription $subscription, string $token): string
    {
        return $this->twig->render( // TODO Exception Handling?
            'pages/campaign/email_verification.text.twig',
            [
                'subscription' => $subscription,
                'token' => $token,
            ]
            );
    }
}
<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use Sulu\Bundle\TestBundle\Testing\SuluTestCase;
use Sulu\Component\DocumentManager\DocumentManagerInterface;

abstract class BaseTestCase extends SuluTestCase
{
    protected function getDocumentManager(): DocumentManagerInterface
    {
        return self::getContainer()->get('sulu_document_manager.document_manager');
    }
}

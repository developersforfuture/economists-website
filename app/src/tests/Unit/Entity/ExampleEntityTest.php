<?php

declare(strict_types=1);

namespace App\Tests\Unit\Entity;

use PHPUnit\Framework\TestCase;

class ExampleEntityTest extends TestCase
{
    public function testExampleOneIsOne(): void
    {
        $this->assertSame(1, 1);
    }
}
